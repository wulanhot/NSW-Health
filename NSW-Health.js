Patients = new Mongo.Collection("patients");

if (Meteor.isClient) {
  
  Template.records.helpers({
    display_records: function () {
      return Patients.find({}, {"sort" : [['ward', 'asc']]});
    }
  });

  
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}

Router.route('/', {
    template: 'records'
});

Router.route('/summary', {
    template: 'summary'
});

Router.configure({
    layoutTemplate: 'main'
});